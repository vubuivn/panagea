<?php include 'include/index-top.php';?>

	
	<main>
		
		<section class="hero_in general dalat">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Already Scheduled Groups In Da Lat</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_60_35">
			
			<div class="row">

				<?php for($i=1;$i<=6;$i++){ ?>				
				<div class="col-xl-4 col-lg-6 col-md-6">
					<?php include 'include/box_grid_dep.php';?>

				</div>
				<!-- /box_grid_dep -->
				<?php } ?>

			</div>
			<!-- /row -->
			
		</div>
		<!-- /container -->
		<section class="hero_in general hoian">
			<div class="wrapper">
				<div class="container">
					<h2 class="fadeInUp"><span></span>Already Scheduled Groups In HOI AN</h2>
				</div>
			</div>
		</section>
		<!--/hero_in-->
		
		<div class="container margin_60_35">
			
			<div class="row">

				<?php for($i=1;$i<=6;$i++){ ?>				
				<div class="col-xl-4 col-lg-6 col-md-6">
					<?php include 'include/box_grid_dep.php';?>

				</div>
				<!-- /box_grid_dep -->
				<?php } ?>

			</div>
			<!-- /row -->
			
		</div>
		<!-- /container -->

		
	</main>
	<!--/main-->

	
<?php include 'include/index-bottom.php';?>	