<?php include 'include/index-top.php';?>

	
	<main>
		<div class="filters_listing sticky_horizontal">
			<div class="container">
				<ul class="clearfix">
					<li>
						<div class="switch-field">
							<input type="radio" id="all" name="listing_filter" value="all" checked data-filter="*" class="selected">
							<label for="all">All</label>
							<input type="radio" id="popular" name="listing_filter" value="popular" data-filter=".popular">
							<label for="popular">Popular</label>
							<input type="radio" id="latest" name="listing_filter" value="latest" data-filter=".latest">
							<label for="latest">Latest</label>
						</div>
					</li>
					<li>
						<div class="layout_view">
							<a href="#0" class="active"><i class="icon-th"></i></a>
							<a href="05.event_list.php"><i class="icon-th-list"></i></a>
						</div>
					</li>
				</ul>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->
		
		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map -->

		<div class="container margin_60_35">
			<div class="col-lg-12">
				<div class="row menu-content">
					<div class="col-sm-6">
						<a href="#" class="item active">School trip</a>
					</div>
					<div class="col-sm-6">
						<a href="#" class="item">Team building event</a>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /custom-search-input-2 -->
			
			<div class="isotope-wrapper">
			<div class="row">

				<?php 
				for($i=1;$i<30;$i++){
					if($i%2==0)
					$iso = 'popular';
					else if($i%3==0)
					$iso = 'latest';
					else
					$iso = 'all';
				?>				
				<div class="col-xl-4 col-lg-6 col-md-6 isotope-item <?php echo $iso; ?>">
					<?php include 'include/box_grid.php';?>

				</div>
				<!-- /box_grid -->
				<?php
				} ?>


			</div>
			<!-- /row -->
			</div>
			<!-- /isotope-wrapper -->
			
			<p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Load more</a></p>
			
		</div>
		<!-- /container -->
		

		
	</main>
	<!--/main-->

	<!-- Masonry Filtering -->
	<script src="js/isotope.min.js"></script>
	<script>
(function($){		
	$(window).load(function(){
	  var $container = $('.isotope-wrapper');
	  $container.isotope({ itemSelector: '.isotope-item', layoutMode: 'masonry' });
	});

	$('.filters_listing').on( 'click', 'input', 'change', function(){
	  var selector = $(this).attr('data-filter');
	  $('.isotope-wrapper').isotope({ filter: selector });
	});
})(jQuery); 	
	</script>
	
<?php include 'include/index-bottom.php';?>	