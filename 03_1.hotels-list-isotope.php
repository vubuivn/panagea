<?php include 'include/index-top.php';?>	
	<main>
		
		<div class="filters_listing sticky_horizontal">
			<div class="container">
				<ul class="clearfix">
					<li>
						<div class="switch-field">
							<input type="radio" id="all" name="listing_filter" value="all" checked data-filter="*" class="selected">
							<label for="all">All</label>
							<input type="radio" id="popular" name="listing_filter" value="popular" data-filter=".popular">
							<label for="popular">Popular</label>
							<input type="radio" id="latest" name="listing_filter" value="latest" data-filter=".latest">
							<label for="latest">Latest</label>
						</div>
					</li>
					<li>
						<div class="layout_view">
							<a href="03.hotels-grid-isotope.php" ><i class="icon-th"></i></a>
							<a href="#0" class="active"><i class="icon-th-list"></i></a>
						</div>
					</li>
				</ul>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->
		
		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map -->

		<div class="container margin_60_35">
			<div class="col-lg-12">
				<div class="row no-gutters custom-search-input-2 inner">
					<div class="col-lg-4">
						<select class="wide">
							<option>Office</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Category</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Type</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-2">
						<input type="submit" class="btn_search" value="Search">
					</div>
				</div>
				<!-- /row -->
			</div>

			<!-- /custom-search-input-2 -->
			<div class="isotope-wrapper">


				<?php 
				for($i=1;$i<30;$i++){
					if($i%2==0)
					$iso = 'popular';
					else if($i%3==0)
					$iso = 'latest';
					else
					$iso = 'all';
				?>					
			<div class="box_list isotope-item <?php echo $iso; ?>">
				<div class="row no-gutters">
					<div class="col-lg-5">
						<figure>
							<small>Parirs Centre</small>
							<a href="04.tour-detail.php">
								<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="lazy lazy-hidden" alt="" width="800" height="533">
							<div class="read_more"><span>Read more</span></div></a>
						</figure>
					</div>
					<div class="col-lg-7">
						<div class="wrapper">
							<a href="#0" class="wish_bt"></a>
							<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
							<h3><a href="04.tour-detail.php">Park Hyatt Hotel</a></h3>
							<p>Dicam diceret ut ius, no epicuri dissentiet philosophia vix. Id usu zril tacimates neglegentur. Eam id legimus torquatos cotidieque, usu decore percipitur definitiones ex, nihil utinam recusabo mel no.</p>
							<span class="price">From <strong>$54</strong> /per person</span>
						</div>

    <ul>
      <li class="duration"><div class="score">Duration :<em>Two Days</em></div></li>
      <li><div class="score"><span>Availability<em>Daily</em></span><strong><?php echo $i; ?></strong></div></li>
    </ul>						
					</div>
				</div>
			</div>
			<!-- /box_list -->
				<?php
				} ?>



			</div>
			<!-- /isotope-wrapper -->
			
			<p class="text-center add_top_60"><a href="#0" class="btn_1 rounded">Load more</a></p>
		
		</div>
		<!-- /container -->

	</main>
	<!--/main-->

	<!-- Masonry Filtering -->
	<script src="js/isotope.min.js"></script>
	<script>
(function($){		
	$(window).load(function(){
	  var $container = $('.isotope-wrapper');
	  $container.isotope({ itemSelector: '.isotope-item', layoutMode: 'masonry' });
	});

	$('.filters_listing').on( 'click', 'input', 'change', function(){
	  var selector = $(this).attr('data-filter');
	  $('.isotope-wrapper').isotope({ filter: selector });
	});
})(jQuery); 	
	</script>
	
<?php include 'include/index-bottom.php';?>	