<?php include 'include/index-top.php';?>	

	<main>
		
		<section class="hero_in general" style="background-image:url('http://ptv-vietnam.com/img/aboutus.jpg')" >
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>About Phat Tire Viet Nam</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="main_title text-left">
					<span><em></em></span>
					<h2>Our Team</h2>
				</div>
				<div class="row justify-content-between">
					<div class="col-lg-6 wow" data-wow-offset="150">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<!-- <img data-lazy-type="image" data-lazy-src="http://www.ptv-vietnam.com/img/aboutus.gif" class="img-fluid lazy lazy-hidden" alt=""> -->
							<img data-lazy-type="image" data-lazy-src="img/aboutus-1.jpg" class="img-fluid lazy lazy-hidden" alt="">
						</figure>
					</div>
					<div class="col-lg-6 line-height-27">
						<p>The Team consists of a group of hard working and fun loving adventure professionals, with a combined experience of over 75 years. We have some veterans that have been with us from the start, and a great group of new rookies that are excited about starting a new career in this industry. The common thing that brings us all together is a love of the outdoors and a passion to provide our clients with the best experience possible.</p><p>Our goal is to provide the greatest value possible by having the best programs, with the best trained, best equipped, safest and most knowledgeable guides in Vietnam. This gives our customers an adrenaline filled experience with the same level of professionalism and safety they would expect back home (wherever home is).</p>
					</div>
				</div>
				<!--/row-->
			</div>
			<!--/container-->
		</div>
		<!--/bg_color_1-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="main_title text-left">
					<span><em></em></span>
					<h2>In safe hands</h2>
				</div>
				<div class="row justify-content-between">
					<div class="col-lg-8 padding_right_60 line-height-27">
						<p>We value our safety record. And in order to improve on our industry beating standards, we constantly upgrade our guide’s skills and training. All guides go through an intensive 30 hour Wilderness First Aid course designed by the Wilderness Medicine Institute and the National Outdoor Leadership School (NOLS). Guides also go through a refresher first aid course each year through Family Medical Practice in Ho Chi Minh City. Our Canyoning and Rock Climbing guides are qualified to Level 3 and certified to Level 2 Abseiling through the Singapore Mountaineering Federation. Our guides completed these courses in Singapore. The guides are also certified to Rope Rescue Level 1 through the Canadian Search and Rescue, Special Services Unit.</p> 
						<p>
						Our White Water Rafting guides go through an intensive SWIFT course with a certified instructor. They are also required to meet very high competency levels for their swimming and undergo a customized guide training program, set up to deal with the specific issues of the rivers they run.
						</p>
					</div>
					<div class="col-lg-4 wow" data-wow-offset="150">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<!-- <img data-lazy-type="image" data-lazy-src="http://www.ptv-vietnam.com/img/aboutus3.jpg" class="img-fluid lazy lazy-hidden" alt=""> -->
							<img data-lazy-type="image" data-lazy-src="img/aboutus-2.jpg" class="img-fluid lazy lazy-hidden" class="img-fluid lazy lazy-hidden" alt="">
						</figure>
							
					</div>
					
				</div>
				<!--/row-->
			</div>
			<!--/container-->
		</div>
		<!--/bg_color_1-->
		
		<div class="bg_color_1 border_all">
			<div class="container margin_80_55 padding_200">
				<div id="carousel" class="partner owl-carousel owl-theme">
					<div class="item">
						<a href="#0">
							<img class="owl-lazy" data-src="img/partner_rescue.jpg" alt=""/>
						</a>
					</div>
					<div class="item">
						<a href="#0">
							<img class="owl-lazy" data-src="img/partner_rescue3.jpg" alt=""/>
						</a>
					</div>
					<div class="item">
						<a href="#0">
							<img class="owl-lazy" data-src="img/partner_smf.png" alt=""/>
						</a>
					</div>
				</div>
			</div>
		<!--/container-->
		</div>
		<!-- bg_color_1 -->
		
		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row">
					<div class="col-md-6 col-lg-6">
						<h3>Da Lat team</h3>
						<p><img src="img/aboutus-tripad.jpg" class="border_all"/></p>
					</div>
					<div class="col-md-6 col-lg-6">
						<h3>Hoi An team</h3>
						<p><img src="img/aboutus-tripad.jpg" class="border_all"/></p>
					</div>

				</div>
			</div>
			<!-- container -->
		</div>
		<!-- bg_color_1	 -->
		
	</main>
	<!--/main-->

<?php include 'include/index-bottom.php';?>	