<?php include 'include/index-top.php';?>	

	<main>
		
		<section class="hero_in general" style="background-image:url('http://ptv-vietnam.com/img/parallex.jpg')">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Contact</h1>
					<p>Still Can't Decide Which Adventure To Take?</p>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row justify-content-between">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-md-6 ">
				
								<h4>Da Lat</h4>
								<ul class="store-item list-icon" data-latlng="11.9493714,108.4347663" data-title="Phat Tire Da lat" data-map="dalat">
									<li><i class="ti-map"></i> <span>109 Nguyen Van Troi Street, Da Lat</span></li>						
									<li><a href="tel://+842633829422"><i class="ti-mobile"></i> +84-263-3829-422</a></li>
									<li><a href="tel://+84263-3820331"><i class="ti-mobile"></i> +84-263-3820-331</a></li>
									<li><a href="tel://+84983-829422"><i class="ti-mobile"></i> +84-983-829-422 (Lam)</a></li>
									<li><a href="mailto:info@phattireventures.com"><i class="ti-email"></i> info@phattireventures.com</a></li>
								</ul>
								<div class="map-wrapper">
					 				<div id="map_dalat" class="map" style="height: 270px;margin-bottom: 100px"></div>
				 				</div>
							</div>
							<div class="col-md-6 ">
								<h4>Hoi An</h4>
								<ul class="store-item list-icon" data-latlng="15.8903002,108.3212589" data-title="Phat Tire Hoi An" data-map="hochiminh">
									<li><i class="ti-map"></i> <span>80 Le Hong Phong Street Hoi An</span></li>						
									<li><a href="tel://+842356539839"><i class="ti-mobile"></i> +84-235-6539-839</a></li>
									<li><a href="tel://+842353917839"><i class="ti-mobile"></i> +84-235-3917-839</a></li>
									<li><a href="tel://+84989686898"><i class="ti-mobile"></i> +84.989-686-898 (Thanh)</a></li>
									<li><a href="mailto:info-hoian@ptv-vietnam.com"><i class="ti-email"></i> info-hoian@ptv-vietnam.com</a></li>
								</ul>
								<div class="map-wrapper">
					 				<div id="map_hochiminh" class="map" style="height: 270px;margin-bottom: 100px"></div>
				 				</div>
							</div>
						</div>
						<!-- /map -->

					</div>
					<div class="col-lg-6">
						<h4>Send a message</h4>
						<!-- <p>Ex quem dicta delicata usu, zril vocibus maiestatis in qui.</p> -->
						<div id="message-contact"></div>
						<form method="post" action="assets/contact.php" id="contactform" autocomplete="off">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Name</label>
										<input class="form-control" type="text" id="name_contact" name="name_contact">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Last name</label>
										<input class="form-control" type="text" id="lastname_contact" name="lastname_contact">
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input class="form-control" type="email" id="email_contact" name="email_contact">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Telephone</label>
										<input class="form-control" type="phone" id="phone_contact" name="phone_contact">
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>How did you hear about us?</label>
										<input class="form-control" type="text" id="question_contact" name="question_contact">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Message</label>
								<textarea class="form-control" id="message_contact" name="message_contact" style="height:77px;resize: none"></textarea>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Are you human? 3 + 1 =</label>
										<input class="form-control" type="text" id="verify_contact" name="verify_contact">
									</div>
								</div>
							</div>
							<p class="add_top_30"><input type="submit" value="Submit" class="btn_1 rounded" id="submit-contact"></p>
						</form>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->


	</main>
	<!--/main-->
	
		<script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLozFL2sjFSOp4AnFeIjoIXliYLJZZYe8&callback=initMaps"></script> 
        <script type="text/javascript">

            function initMaps(){

            	 var mapOptions = {
            	 	disableDefaultUI: true,
			   		streetViewControl: false,
			   		zoom: 13,
			    };

			    if($('.store-item').length > 0){


				    $('.store-item').each(function(e) {
				    	var mapid  = $(this).data('map');
				    	var googlemap = new google.maps.Map(document.getElementById("map_"+mapid), mapOptions);

				    	var latlng = $(this).data("latlng");
				    	var title = $(this).data("title");

				    	var  infoWindow = new google.maps.InfoWindow({maxWidth: 350});

				    	latlng = latlng.split(",");
				        var lat = Number(latlng[0]);
				        var lng = Number(latlng[1]);

				        var marker = new google.maps.Marker({
				            position: {lat: lat, lng: lng},
				            map: googlemap
				        });

				        var pos = {
				            lat: lat,
				            lng: lng
				        };
				       	infoWindow.setPosition(pos);
				        infoWindow.setContent(title);
				 
				        infoWindow.open(googlemap);
				        googlemap.setCenter(pos);

			    	});
 				}
	        }

        </script>
<?php include 'include/index-bottom.php';?>	