<?php include 'include/index-top.php';?>

	    <main>
        <section class="hero_in general start_bg_zoom" style="background-image:url('http://ptvtravel.mangoads.com.vn/front/img/parallex.jpg')">
            <div class="wrapper">
                <div class="container">
                    <h1 class="fadeInUp animated"><span></span>News</h1>
                </div>
            </div>
        </section>
        <!--/hero_in-->

        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-9">
                    <div class="bloglist singlepost">
                        <h1>Canyoning in Da Lat with Phat Tire Ventures</h1>
                        <div class="postmeta">
                            <ul>
                                <li><a><i class="icon_clock_alt"></i> 18 Feb. 2019</a></li>
                            </ul>
                        </div>
                        <!-- /post meta -->
                        <div class="post-content">
  

<p><img alt="" height="600" src="http://ptvtravel.mangoads.com.vn/upload/cke/canyoning4-5c78d25167.jpg" width="800" /></p>

<p>Other sections of the day were rather leisurely: lay back, relax, and let the river carry you.</p>

<p><img alt="" height="600" src="http://ptvtravel.mangoads.com.vn/upload/cke/canyoning5-c8ded9fa45.jpg" width="800" /></p>

<p>The third obstacle is a free jump into a big pool. At roughly 7 meters high {23 feet}, it’s high enough to give you an adrenaline rush but low enough to not be terrifying! After a few jumps, it’s time to continue to the final obstacle…</p>

<p>The Washing Machine is the fourth and final obstacle of the day. It may not looks as treacherous as it actually is! You have to experience it to really get it. Begin at the top and to the side of the waterfall and abseil down. About mid-way down, you encounter the waterfall, which is so strong that it begins to spin you around, just like a washing &nbsp;machine. Keep abseiling down through the water, which will push you under the pool and out of the narrow passageway!</p>

<p>All-in-all, it was an awesome day! Canyoning in Dalat with Phat Tire Ventures is extremely organized, fun and safe. I also really enjoyed that I was a part of such a small group. At the very beginning, another tour came behind us and they easily had 15 or more people in their group. That would mean a lot of waiting around while everyone does the obstacles individually. Instead, we were able to do each obstacle a couple of times! We were also alone the entire day once we got past the first obstacle. Having the canyon to ourselves was amazing. The scenery is half of what makes the experience so amazing to begin with. The small group really enabled us to take it all in.</p>

                        </div>
                        <!-- /post -->

                    </div>
                    <!-- /single-post -->
                </div>
                <!-- /col -->

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </main>

	<!-- Map 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLozFL2sjFSOp4AnFeIjoIXliYLJZZYe8"></script>
<script src="js/map_single_tour.js"></script>
<script src="js/infobox.js"></script>
	-->

	<!-- DATEPICKER  -->
	<script>
	$(function() {
	  $('input[name="dates"]').daterangepicker({
		  autoUpdateInput: false,
		  opens: 'left',
		  locale: {
			  cancelLabel: 'Clear'
		  }
	  });
	  $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
		  $(this).val(picker.startDate.format('MM-DD-YY') + ' > ' + picker.endDate.format('MM-DD-YY'));
	  });
	  $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
		  $(this).val('');
	  });
	});
	</script>
	

	
	<!-- INSTAGRAM FEED  -->
	<script>
		$(window).on('load', function() {
			"use strict";
			$.instagramFeed({
				'username': 'thelouvremuseum',
				'container': "#instagram-feed",
				'display_profile': false,
				'display_biography': false,
				'display_gallery': true,
				'get_raw_json': false,
				'callback': null,
				'styling': true,
				'items': 12,
				'items_per_row': 6,
				'margin': 1
			});
		});
	</script>
	
<?php include 'include/index-bottom.php';?>	