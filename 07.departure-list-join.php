<?php include 'include/index-top.php';?>	

	<main>
		
		<section class="hero_in general" style="background-image:url('http://ptv-vietnam.com/img/parallex.jpg')">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Join Group</h1>
					<p>1 Day Bike From Hoi An To Hue</p>
					<p>03-Apr-19</p>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row justify-content-between">
					<div class="col-lg-12">
						<!-- <h4>Send a message</h4> -->
						<!-- <p>Ex quem dicta delicata usu, zril vocibus maiestatis in qui.</p> -->
						<form method="post" action="#" id="joinGroup" autocomplete="off">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Your Trip</label>
										<input type="text" class="form-control" placeholder="" readonly="readonly" id="tour.name" name="tour.name" value="1 Day Bike From Hoi An To Hue">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Your Day</label>
										<div class="input-group mb-3">
										  <input type="text" class="form-control" placeholder="tourDate" aria-label="Recipient's username" readonly="readonly" value="03-Apr-19">
										  <div class="input-group-append">
										    <span class="input-group-text" id="basic-addon2"><i class="icon-calendar"></i></span>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Number of Persons in your Group
											<span class="required"> * </span>
										</label>
										<input type="number" name="pax" class="form-control" placeholder="" data-required="1" id="pax" value="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Name</label>
										<input class="form-control" type="text" id="name_contact" name="name_contact">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Email</label>
										<input class="form-control" type="email" id="email_contact" name="email_contact">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Telephone</label>
										<input class="form-control" type="phone" id="phone_contact" name="phone_contact">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>How did you hear about us?</label>
										<input class="form-control" type="text" id="question_contact" name="question_contact">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Additional Information</label>
								<textarea class="form-control" id="message_contact" name="message_contact" style="height:77px;resize: none"></textarea>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Are you human? 3 + 1 =</label>
										<input class="form-control" type="text" id="verify_contact" name="verify_contact">
									</div>
								</div>
							</div>
							<p class="add_top_30"><input type="submit" value="Submit" class="btn_1 rounded" id="submit-contact"></p>
						</form>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->


	</main>
	<!--/main-->
	
<?php include 'include/index-bottom.php';?>	