<?php include 'include/index-top-home.php';?>

	<main>
		<section class="hero_single version_2 ">
			<div class="wrapper">
				<div class="container">
					<h3>Book unique experiences</h3>
					<p>Expolore top rated tours, hotels and restaurants around the world</p>
					<form>
						<div class="row no-gutters custom-search-input-2">
							<div class="col-lg-10">
								<div class="form-group">
									<input class="form-control" type="text" placeholder="Search for adventures">
									<i class="icon_pin_alt"></i>
								</div>
							</div>
							<div class="col-lg-2">
								<input type="submit" class="btn_search" value="Search">
							</div>
						</div>
						<!-- /row -->
					</form>
				</div>
			</div>


			<div class="wrapVideoBg "  data-video="EthqIhPtd2I"> 
				<iframe frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="560" height="315" src="https://www.youtube.com/embed/EthqIhPtd2I?autoplay=1&amp;controls=0&amp;disablekb=1&amp;hl=ru-ru&amp;loop=1&amp;modestbranding=1&amp;showinfo=0&amp;autohide=0&amp;color=white&amp;iv_load_policy=3&amp;theme=light&amp;mute=0&amp;rel=0&amp;enablejsapi=1"  ></iframe> 
			</div>
			<div class="bgimg">
				<img data-lazy-type="image" data-lazy-src="img/home_section_1.jpg" class="lazy lazy-hidden" alt="">
			</div>

		</section>
		<!-- /hero_single -->






		<div class="container-fluid margin_80_0">
			<div class="main_title_2">
				<span><em></em></span>
				<h2>Our Popular Tours</h2>
				<p>Amazing Adventures, Trips and Holidays in Vietnam</p>
			</div>
			<div id="reccomended" class="owl-carousel owl-theme">


				<?php 
				for($i=1;$i<10;$i++){
				?>				
				<div class="item">
					<?php include 'include/box_grid.php';?>
				</div>
				<!-- /box_grid -->
				<?php
				} ?>


			</div>
			<!-- /carousel -->
			<div class="container">
				<p class="btn_home_align"><a href="#" class="btn_1 rounded">View all Tours</a></p>
			</div>
			<!-- /container -->
			<hr class="large">
		</div>
		<!-- /container -->
		
		<div class="container-fluid margin_30_95 pl-lg-5 pr-lg-5">
			<section class="add_bottom_45">
				<div class="main_title_3">
					<span><em></em></span>
					<h2>Da Lat </h2>
					<p>View Adventures That Depart From Da Lat</p>
				</div>

			<div  class="slide_4 owl-carousel owl-theme">
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img   data-src="img/Canyoning_Short_Option.jpeg" class="owl-lazy" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Canyoning $72 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->

				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Canyoning.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Canyoning Short Option $57 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Hike_Kayak.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Jungle Fever Trekking $91 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Jungle_Boogie_Trekking.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Jungle Boogie Trekking $41 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->

				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Hike & Kayak $45 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/School_Trip.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>School Trip </h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
			</div>


				<a href="#"><strong>View all (157) <i class="arrow_carrot-right"></i></strong></a>
			</section>
			<!-- /section -->
			
			<section>
				<div class="main_title_3">
					<span><em></em></span>
					<h2>Hoi An</h2>
					<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
				</div>




			<div  class="slide_4 owl-carousel owl-theme">
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/1_Day_Bike_From_Hoi_An_To_Hue.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>View Adventures That Depart From Hoi An</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->

				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Holy_Moly_Biking.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Marble Mountain Excursion $42 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Marble_Mountain_Descent.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Holy Moly Biking$59 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Marble_Mountain_Excursion.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Rock Climbing At Marble Mountain $61 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->

				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Ride_Into_The_Past.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Marble Mountain Descent $61 US</h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
				<div class="item">
						<a href="04.tour-detail.php" class="grid_item">
							<figure>
								<div class="score"><strong>8.9</strong></div>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Rock_Climbing_At_Marble_Mountain.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>
								<div class="info">
									<div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
									<h3>Ride Into The Past $59 US </h3>
								</div>
							</figure>
						</a>
				</div>
				<!-- /item -->
			</div>



				<a href="#"><strong>View all (157) <i class="arrow_carrot-right"></i></strong></a>
			</section>
			<!-- /section -->
		</div>
		<!-- /container -->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="main_title_2">
					<span><em></em></span>
					<h3>News</h3>
					<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<a class="box_news" href="#0">
							<figure>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Rock_Climbing_At_Marble_Mountain.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>								
								<figcaption><strong>28</strong>Dec</figcaption>
							</figure>
							<ul>
								<li>Mark Twain</li>
								<li>20.11.2017</li>
							</ul>
							<h4>Canyoning adventures in Da Lat, Viet Nam: Things you need to know</h4>
							<p>Things you need to know before joining any Canyoning Adventures in Da Lat, Vietnam to maximize your experience and safety.
....</p>
						</a>
					</div>
					<!-- /box_news -->

					<div class="col-lg-6">
						<a class="box_news" href="#0">
							<figure>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Jungle_Boogie_Trekking.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>								
								<figcaption><strong>28</strong>Dec</figcaption>
							</figure>
							<ul>
								<li>Mark Twain</li>
								<li>20.11.2017</li>
							</ul>
							<h4>Canyoning adventures in Da Lat, Viet Nam: Things you need to know</h4>
							<p>Things you need to know before joining any Canyoning Adventures in Da Lat, Vietnam to maximize your experience and safety.
....</p>
						</a>
					</div>
					<!-- /box_news -->

					<div class="col-lg-6">
						<a class="box_news" href="#0">
							<figure>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>								
								<figcaption><strong>28</strong>Dec</figcaption>
							</figure>
							<ul>
								<li>Mark Twain</li>
								<li>20.11.2017</li>
							</ul>
							<h4>Canyoning adventures in Da Lat, Viet Nam: Things you need to know</h4>
							<p>Things you need to know before joining any Canyoning Adventures in Da Lat, Vietnam to maximize your experience and safety.
....</p>
						</a>
					</div>
					<!-- /box_news -->

					<div class="col-lg-6">
						<a class="box_news" href="#0">
							<figure>
								<div class="img tRes_70">
									<img data-lazy-type="image" data-lazy-src="img/Team_Building.jpeg" class="img-fluid lazy lazy-hidden" alt="">
								</div>								
								<figcaption><strong>28</strong>Dec</figcaption>
							</figure>
							<ul>
								<li>Mark Twain</li>
								<li>20.11.2017</li>
							</ul>
							<h4>Canyoning adventures in Da Lat, Viet Nam: Things you need to know</h4>
							<p>Things you need to know before joining any Canyoning Adventures in Da Lat, Vietnam to maximize your experience and safety.
....</p>
						</a>
					</div>
					<!-- /box_news -->
				</div>
				<!-- /row -->
				<p class="btn_home_align"><a href="#" class="btn_1 rounded">View all news</a></p>
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->


		<div class=" reviews-part">
			<div class="container margin_80_55">
				<div class="main_title_2">
					<span><em></em></span>
					<h3>Recent Reviews</h3>
					<p>From Tripadvisor</p>
				</div>

				<div  class="reviews-content slide_2 owl-carousel owl-theme">
					
				

				<?php 
				for($i=1;$i<10;$i++){ ?>
					<div class="item">


<div class="item-review">
    <img class="avatar" alt="" src="https://media-cdn.tripadvisor.com/media/photo-l/0f/ee/0d/e7/glen-h.jpg" >
    <div class="message">
        <span class="arrow"> </span>
        <a href="javascript:;" class="name">Glen H</a>
        <span class="body">Abseiling Marble Mountain was an amazing way to experience such an impressive place. My girlfriend was absolutely terrified and both guides where very supportive and helpful. With their help and support my girlfriend completed all 3. The gear that was used is world class and...More</span>
        <div>
        	<div>
        		<i class="icon_star"> </i>
        		<i class="icon_star"> </i>
        		<i class="icon_star"> </i>
        		<i class="icon_star"> </i>
        		<i class="icon_star"> </i>
  
        	</div> <a href="https://www.tripadvisor.com/ShowUserReviews-g298082-d1737266-r502737519-Phat_Tire_Ventures-Hoi_An_Quang_Nam_Province.html#REVIEWS">Read on Tripadvisor</a></div>
    </div>
</div>

						
					</div>

				<?php
				}?>
				</div>




			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->


		<div class="call_section lazy lazy-hidden" data-lazy-type="bg" data-lazy-src="img/White_Water_Rafting.jpg" >
			<div class="container clearfix">
				<div class="col-lg-5 col-md-6 float-right wow" data-wow-offset="250">
					<div class="block-reveal">
						<div class="block-vertical"></div>
						<div class="box_1">
							<h3>Scheduled tours</h3>
							<p>Ius cu tamquam persequeris, eu veniam apeirian platonem qui, id aliquip voluptatibus pri. Ei mea primis ornatus disputationi. Menandri erroribus cu per, duo solet congue ut. </p>
							<a href="#0" class="btn_1 rounded">Join groups</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/call_section-->
	</main>
	<!-- /main -->

	
	<!-- INPUT QUANTITY  -->

<?php include 'include/index-bottom.php';?>