<?php include 'include/index-top.php';?>
	<main>
		<div class="filters_listing sticky_horizontal hide-mobile">  <!--Detect PC and Tablet-->
			<div class="container">

				<div class="row no-gutters custom-search-input-2 inner ">
					<div class="col-lg-4">
						<select class="wide">
							<option>Office</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Category</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Type</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-2">
						<input type="submit" class="btn_search" value="Search">
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->
		
		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map -->

		<div class="container margin_60_35">

				<div class="row no-gutters custom-search-input-2 inner mb-30 show-mobile"> <!--Detect only mobile-->
					<div class="col-lg-4">
						<select class="wide">
							<option>Office</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Category</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-3">
						<select class="wide">
							<option>Tour Type</option>	
							<option>Da Lat</option>
							<option>Hoi An</option>
						</select>
					</div>
					<div class="col-lg-2">
						<input type="submit" class="btn_search" value="Search">
					</div>
				</div>
				<!-- /row -->



			
			<div class="">
			<div class="row isotope-wrapper">

				<?php 
				for($i=1;$i<30;$i++){
					if($i%2==0)
					$iso = 'popular';
					else if($i%3==0)
					$iso = 'latest';
					else
					$iso = 'all';
				?>				
				<div class="col-xl-4 col-lg-6 col-md-6 isotope-item <?php echo $iso; ?>">
					<?php include 'include/box_grid.php';?>

				</div>
				<!-- /box_grid -->
				<?php
				} ?>


			</div>
			<!-- /row -->
			</div>
			<!-- /isotope-wrapper -->
			
			<p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Load more</a></p>
			
		</div>
		<!-- /container -->
		

		
	</main>
	<!--/main-->

	<!-- Masonry Filtering -->
	<script src="js/isotope.min.js"></script>
	<script>
(function($){		
	$(window).load(function(){
	  var $container = $('.isotope-wrapper');
	  $container.isotope({ itemSelector: '.isotope-item', layoutMode: 'masonry' });
	});

	$('.filters_listing').on( 'click', 'input', 'change', function(){
	  var selector = $(this).attr('data-filter');
	  $('.isotope-wrapper').isotope({ filter: selector });
	});
})(jQuery); 	
	</script>
	
<?php include 'include/index-bottom.php';?>	