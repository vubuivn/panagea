
	<footer>
		<div class="container margin_60_35">
			<div class="row">
				<div class="col-sm-3 col-md-4">
					<p><img data-lazy-type="image" data-lazy-src="img/logo.png" class="lazy lazy-hidden"    height="50" data-retina="true" alt=""></p>
					<p>PTV Vietnam is the longest running adventure sports outfitter in Vietnam specializing in mountain biking, white water rafting, trekking, rock climbing and canyoning. We have offices in both Dalat and Hoi An.</p>
					<div class="follow_us">
						<ul>
							<li>Follow us</li>
							<li><a href="#0"><i class="ti-facebook"></i></a></li>
							<li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
							<li><a href="#0"><i class="ti-google"></i></a></li>
							<li><a href="#0"><i class="ti-pinterest"></i></a></li>
							<li><a href="#0"><i class="ti-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3 col-md-2">
					<h5>Useful links</h5>
					<ul class="links">
						<li><a href="#0">About us</a></li>
						<li><a href="#0">Adventures</a></li>
						<li><a href="#0">Events</a></li>
						<li><a href="#0">Scheduled trips</a></li>
						<li><a href="#0">News</a></li>
						<li><a href="#0">Contact</a></li>

						
					</ul>
				</div>

				<div class="col-lg-3 col-md-6">
					<h5>Contact Us in Da Lat</h5>
					<ul class="contacts">
						<li><i class="ti-map"></i> <span>109 Nguyen Van Troi Street, Da Lat</span></li>						
						<li><a href="tel://+842633829422"><i class="ti-mobile"></i> +84-263-3829-422</a></li>
						<li><a href="tel://+84263-3820331"><i class="ti-mobile"></i> +84-263-3820-331</a></li>
						<li><a href="tel://+84983-829422"><i class="ti-mobile"></i> +84-983-829-422 (Lam)</a></li>
						<li><a href="mailto:info@phattireventures.com"><i class="ti-email"></i> info@phattireventures.com</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h5>Contact Us in Hoi An</h5>
					<ul class="contacts">
						<li><i class="ti-map"></i> <span>80 Le Hong Phong Street Hoi An</span></li>						
						<li><a href="tel://+842356539839"><i class="ti-mobile"></i> +84-235-6539-839</a></li>
						<li><a href="tel://+842353917839"><i class="ti-mobile"></i> +84-235-3917-839</a></li>
						<li><a href="tel://+84989686898"><i class="ti-mobile"></i> +84.989-686-898 (Thanh)</a></li>
						<li><a href="mailto:info-hoian@ptv-vietnam.com"><i class="ti-email"></i> info-hoian@ptv-vietnam.com</a></li>
					</ul>



				</div>				
			</div>
			<!--/row-->

		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<!-- Sign In Popup -->
	<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="small-dialog-header">
			<h3>Sign In</h3>
		</div>
		<form>
			<div class="sign-in-wrapper">
				<a href="#0" class="social_bt facebook">Login with Facebook</a>
				<a href="#0" class="social_bt google">Login with Google</a>
				<div class="divider"><span>Or</span></div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value="">
					<i class="icon_lock_alt"></i>
				</div>
				<div class="clearfix add_bottom_15">
					<div class="checkboxes float-left">
						<label class="container_check">Remember me
						  <input type="checkbox">
						  <span class="checkmark"></span>
						</label>
					</div>
					<div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
				</div>
				<div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
				<div class="text-center">
					Don’t have an account? <a href="#0">Sign up</a>
				</div>
				<div id="forgot_pw">
					<div class="form-group">
						<label>Please confirm login email below</label>
						<input type="email" class="form-control" name="email_forgot" id="email_forgot">
						<i class="icon_mail_alt"></i>
					</div>
					<p>You will receive an email containing a link allowing you to reset your password to a new preferred one.</p>
					<div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
				</div>
			</div>
		</form>
		<!--form -->
	</div>
	<!-- /Sign In Popup -->
	
	<div id="toTop"></div><!-- Back to top button -->
	
	<!-- COMMON SCRIPTS -->

    <script src="js/common_scripts.js"></script>
    <script src="js/main.js"></script>
	<script src="assets/validate.js"></script>
	

	
</body>
</html>