<header class="header menu_fixed ">
  <div id="preloader"><div data-loader="circle-side"></div></div><!-- /Page Preload -->
  <div id="logo">
    <a href="index.php">
      <img  src="img/logo.png" height="60"  alt="" class="logo_normal ">
      <img  src="img/logo.png" height="60"  alt="" class="logo_sticky ">
    </a>
  </div>


  <div class="follow_us">
    <ul>
      <li><a href="#0"><i class="ti-facebook"></i></a></li>
      <li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
      <li><a href="#0"><i class="ti-google"></i></a></li>
      <li><a href="#0"><i class="ti-pinterest"></i></a></li>
      <li><a href="#0"><i class="ti-instagram"></i></a></li>
    </ul>
  </div>   
  <!-- /top_menu -->
  <a href="#menu" class="btn_mobile">
    <div class="hamburger hamburger--spin" id="hamburger">
      <div class="hamburger-box">
        <div class="hamburger-inner"></div>
      </div>
    </div>
  </a>
  <nav id="menu" class="main-menu">
    <ul>
      <li><span><a href="index.php">Home</a></span> </li>
      <li><span><a href="03.hotels-grid-isotope.php">Adventures</a></span></li>
      <li><span><a href="05.event.php">Events</a></span></li>
      <li><span><a href="07.departure-list.php">Scheduled trips</a></span></li>
      <li><span><a href="02.about.php">About us</a></span></li>
      <li><span><a href="06.contact.php">Contact</a></span></li>
    </ul>
  </nav>
</header>
<!-- /header -->