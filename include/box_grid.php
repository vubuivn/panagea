  <div class="box_grid">
    <figure>
      <a href="#0" class="wish_bt"></a>
      <a href="04.tour-detail.php" class="tRes_70"><img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="lazy lazy-hidden" alt="" ><div class="read_more"><span>Read more</span></div></a>
      <small>Paris Centre</small>
    </figure>
    <div class="wrapper">
      <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div>
      <h3><a href="04.tour-detail.php">Park Hyatt Hotel <?php echo $i; ?></a></h3>
      <p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>
      <span class="price">From <strong>$54</strong> /per person</span>
    </div>
    <ul>
      <li class="duration"><div class="score">Duration<em>Two Days</em></div></li>
      <li><div class="score"><span>Availability<em>Daily</em></span><strong><?php echo $i; ?></strong></div></li>
    </ul>
  </div>