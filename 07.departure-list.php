<?php include 'include/index-top.php';?>

	
	<main>
		
		<section class="hero_in general dalat">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Already Scheduled Groups In Da Lat</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_60_35">

			<?php for( $i=1; $i <= 11; $i++) :?>
				<div class="box_list depature">
					<div class="row box-item">
			        	<div class="col-xs-12 col-sm-3">
			        		<figure>
			        			<a href="#">
				        			<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="lazy lazy-hidden" alt="" width="300" height="150">
			        			</a>
			        		</figure>
			        	</div>
			        	<div class="col-xs-12 col-sm-2 box-item-cat">
			        		<strong class="label">Adventure</strong>
			        		<div class="categories">1 Day Bike From Hoi An To Hue</div>
			        	</div>			        	
			        	<div class="col-xs-12 col-sm-2 box-item-date">
			        		<strong class="label">Date</strong>
			        		<div class="date">23-Jan-19</div>
			        		
			        	</div>

			        	<div class="col-xs-12 col-sm-2 box-item-pax">
			        		<strong class="label">Current Pax</strong>
			        		<div class="pax">4</div>
			        	</div>
			        	<div class="col-xs-12 col-sm-1 box-item-price">
			        		<strong class="label">USD /Pax</strong>
			        		<div class="price">98</div>
			        	</div>
			        	<div class="col-xs-12 col-sm-2 box-item-action">
			        		<div class="btn-wrapper">
			        			  <a href="07.departure-list-join.php" class="btn_1 rounded add_top_20">Join Group</a>
			        		</div>
			        	</div>
					</div>
				</div>
				<!-- box_list depature -->
			<?php endfor?>
			
		</div>
		<!-- /container -->
		<section class="hero_in general hoian">
			<div class="wrapper">
				<div class="container">
					<h2 class="fadeInUp"><span></span>Already Scheduled Groups In HOI AN</h2>
				</div>
			</div>
		</section>
		<!--/hero_in-->
		
		<div class="container margin_60_35">
			
				<?php for( $i=1; $i <= 9; $i++) :?>
					<div class="box_list depature">
					<div class="row box-item">
			        	<div class="col-xs-12 col-sm-3">
			        		<figure>
			        			<a href="#">
				        			<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="lazy lazy-hidden" alt="" width="300" height="150">
			        			</a>
			        		</figure>
			        	</div>
			        	<div class="col-xs-12 col-sm-2 box-item-cat">
			        		<strong class="label">Adventure</strong>
			        		<div class="categories">1 Day Bike From Hoi An To Hue</div>
			        	</div>
			        	<div class="col-xs-12 col-sm-2 box-item-date">
			        		<strong class="label">Date</strong>
			        		<div class="date">23-Jan-19</div>
			        		
			        	</div>

			        	<div class="col-xs-12 col-sm-2 box-item-pax">
			        		<strong class="label">Current Pax</strong>
			        		<div class="pax">4</div>
			        	</div>
			        	<div class="col-xs-12 col-sm-1 box-item-price">
			        		<strong class="label">USD /Pax</strong>
			        		<div class="price">98</div>
			        	</div>
			        	<div class="col-xs-12 col-sm-2 box-item-action">
			        		<div class="btn-wrapper">
			        			  <a href="07.departure-list-join.php" class="btn_1 rounded add_top_20">Join Group</a>
			        		</div>
			        	</div>
					</div>
				</div>
					<!-- box_list depature -->
				<?php endfor?>
			
		</div>
		<!-- /container -->

		
	</main>
	<!--/main-->

	
<?php include 'include/index-bottom.php';?>	