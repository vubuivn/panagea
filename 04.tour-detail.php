<?php include 'include/index-top.php';?>

	<main class="page-tour-detail">

		<div class="bg_color_1">
			<nav class="secondary_nav sticky_horizontal">
				<div class="container">
					<div class="pull-right"><a href="#" class="btn_1 rounded">Want To Join Other Groups?</a></div>
					<ul class="clearfix">
						<li><a href="#overview" class="active">Overview</a></li>
						<li><a href="#information">Information</a></li>
						<li><a href="#location">Route Map</a></li>
						<li><a href="#tourdetail">Tour Detail</a></li>

					</ul>
				</div>
			</nav>
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-lg-8">
							<div id="featureImg" class="owl-carousel owl-theme slide_1">
								<?php 
								for($i=1;$i<4;$i++){
								?>				
								<div class="item">
									<div class="tRes_70">
										<img  data-src="img/Jungle_Fever_Trekking.jpeg" class="owl-lazy" alt="" >
									</div>
								</div>
								<!-- /box_grid -->
								<?php
								} ?>
							</div>
						<section id="overview">

							<h2>Description</h2>
							<p>Per consequat adolescens ex, cu nibh commune <strong>temporibus vim</strong>, ad sumo viris eloquentiam sed. Mea appareat omittantur eloquentiam ad, nam ei quas oportere democritum. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
							<p>Cum et probo menandri. Officiis consulatu pro et, ne sea sale invidunt, sed ut sint <strong>blandit</strong> efficiendi. Atomorum explicari eu qui, est enim quaerendum te. Quo harum viris id. Per ne quando dolore evertitur, pro ad cibo commune.</p>

							<hr>
							<p>Mea appareat omittantur eloquentiam ad, nam ei quas <strong>oportere democritum</strong>. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
							<div class="row">
								<div class="col-lg-6">
									<ul class="bullets">
										<li>Dolorem mediocritatem</li>
										<li>Mea appareat</li>
										<li>Prima causae</li>
										<li>Singulis indoctum</li>
									</ul>
								</div>
								<div class="col-lg-6">
									<ul class="bullets">
										<li>Timeam inimicus</li>
										<li>Oportere democritum</li>
										<li>Cetero inermis</li>
										<li>Pertinacia eum</li>
									</ul>
								</div>
							</div>
							<!-- /row -->
							
							<hr>

						</section>
						<!-- /section -->

						<section id="information">
							<h2>Information</h2>

								<table class="table table-bordered table-striped"> 
									<thead> 
										<tr> 
											<th width="150">Class</th> 
											<th>Description</th> 
										</tr> 
									</thead> 
									<tbody> 
								<?php
								for($i=1;$i<6;$i++){ ?>		
										<tr> 
											<th scope="row"> Depart From: </th> 
											<td>109 Nguyen Van Troi Street, or we will pick you up at your hotel</td> 
										</tr> 
										<tr> 
											<th scope="row"> What is included: </th> 
											<td>English speaking professionally trained guide, all necessary forest and tourist permits, first aid kit, snack, purified drinking water</td> 
										</tr> 
								<?php
								} ?>
									</tbody> 
								</table>
							<!-- /review-container -->
						</section>
						<!-- /section -->


						<section id="location">
							<h2>Location</h2>

							<div><a th:download="download" href="http://www.ptv-vietnam.com/routemaps/canyoning.kmz">Download .kmz file for Google Earth</a></div>
							<div><a th:download="download" href="http://www.ptv-vietnam.com/routemaps/canyoning.kmz">Download .kmz file for Google Earth</a></div>
							<br>


							<div class="tRes_16_9">
								<img data-lazy-type="image" data-lazy-src="img/Jungle_Fever_Trekking.jpeg" class="lazy lazy-hidden" alt="" >
							</div>

							<!--<div id="map" class="map map_single add_bottom_30"></div>-->


							<hr>
						</section>
						<!-- /section -->

					
						<section id="tourdetail">
							<h2>Tour Detail</h2>

								<table class="table table-bordered table-striped"> 
									<thead> 
										<tr> 
											<th>Class</th> 
											<th>Description</th> 
										</tr> 
									</thead> 
									<tbody> 
										<tr> 
											<th scope="row"> Availability </th> 
											<td>Daily</td> 
										</tr> 
										<tr> 
											<th scope="row"> Minimum Participant </th> 
											<td>2</td> 
										</tr> 
										<tr> 
											<th scope="row"> Duration </th> 
											<td>5 Hours</td> 
										</tr> 
										<tr> 
											<th scope="row"> Activities </th> 
											<td>Hiking, Rappelling, Tyrolean Traverse, Water Sliding And Swimming</td> 
										</tr> 
										<tr> 
											<th scope="row"> Guide Language </th> 
											<td>English & Vietnamese</td> 
										</tr> 
										<tr> 
											<th scope="row"> Adventure Grade </th> 
											<td>5 – Moderate On Trekking And 5-7 On Canyoning</td> 
										</tr> 
										<tr> 
											<th scope="row"> Departure From </th> 
											<td>109 Nguyen Van Troi Street, Or We Will Pick You Up At Your Hotel</td> 
										</tr> 

									</tbody> 
								</table>
							<!-- /review-container -->

							<hr>							
						</section>
						<!-- /section -->				

						<div>
						<ul class="share-buttons">
							<li><a class="fb-share" href="#0"><i class="social_facebook"></i> Share</a></li>
							<li><a class="twitter-share" href="#0"><i class="social_twitter"></i> Tweet</a></li>
							<li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
						</ul>							
						</div>		



					


					</div>
					<!-- /col -->
					
					<aside class="col-lg-4" id="sidebar">
						<div class="box_detail booking">
							<div class="price">
								<span>45$ <small>person</small></span>
								<h4 class="title">Da Lat</h4>
							</div>

							<div class="form-group">
								<input class="form-control" type="text"  placeholder="Your Name *">
							</div>
							<div class="form-group">
								<input class="form-control" type="email"  placeholder="Your Email *">
							</div>
							<div class="form-group">
								<input class="form-control" type="text"  placeholder="Your Phone">
							</div>
							<div class="form-group">
								<input class="form-control" type="text"  placeholder="Your Trip">
							</div>
							<div class="form-group">
								<input class="form-control" type="text" name="dates" placeholder="When  *..">
								<i class="icon_calendar"></i>
							</div>							
							<div class="form-group">
								<input class="form-control" type="text"  placeholder="Number of Persons in your Group  *">
							</div>

							<div class="form-group">
								<input class="form-control" type="text"  placeholder="How did you hear about us?">
							</div>
							<div class="form-group">
								<textarea style="height: 100px;" class="form-control" name="" id="" cols="30" rows="10" placeholder="Additional Information"></textarea>
							</div>
							<div class="form-group text-center">
								<img src="img/captchar.png"  alt="" >
							</div>							
							<a href="#" class="btn_1 full-width purchase">Book Now</a>

							<div class="text-center"><small>No money charged in this step</small></div>
						</div>

					</aside>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->


	<!-- Map 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLozFL2sjFSOp4AnFeIjoIXliYLJZZYe8"></script>
<script src="js/map_single_tour.js"></script>
<script src="js/infobox.js"></script>
	-->

	<!-- DATEPICKER  -->
	<script>
	$(function() {
	  // $('input[name="dates"]').daterangepicker({
		 //  autoUpdateInput: false,
		 //  opens: 'left',
		 //  singleDatePicker: true,
		 //  // locale: {
			//  //  cancelLabel: 'Clear'
		 //  // }
	  // });
	  // $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
		 //  $(this).val(picker.startDate.format('MM-DD-YY') + ' > ' + picker.endDate.format('MM-DD-YY'));
	  // });
	  // $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
		 //  $(this).val('');
	  // });

	  $('input[name="dates"]').daterangepicker({
	  	singleDatePicker: true
	  });
	});
	</script>
	

	
	<!-- INSTAGRAM FEED  -->
	<script>
		$(window).on('load', function() {
			"use strict";
			$.instagramFeed({
				'username': 'thelouvremuseum',
				'container': "#instagram-feed",
				'display_profile': false,
				'display_biography': false,
				'display_gallery': true,
				'get_raw_json': false,
				'callback': null,
				'styling': true,
				'items': 12,
				'items_per_row': 6,
				'margin': 1
			});
		});
	</script>
	
<?php include 'include/index-bottom.php';?>	